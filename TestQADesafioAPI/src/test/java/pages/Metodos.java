package pages;

import static org.junit.Assert.assertEquals;
import org.json.simple.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Metodos {

	public void parametersPostPut(String nome, String cpf, String email, int valor, int parcelas, boolean seguro,
			String passo, int statusCode) {

		RequestSpecification request = RestAssured.given().header("Content-Type", "application/json");

		JSONObject json = new JSONObject();

		json.put("nome", nome);
		json.put("cpf", cpf);
		json.put("email", email);
		json.put("valor", valor);
		json.put("parcelas", parcelas);
		json.put("seguro", seguro);

		request.body(json.toJSONString());

		if (passo == "post") {

			System.out.println("Teste simulações:\n");

			Response response = request.post("http://localhost:8080/api/v1/simulacoes");

			if (statusCode == 201) {

				assertEquals(statusCode, response.getStatusCode());
				System.out.println("POST: Cadastro com sucesso\n");
				System.out.println("Status code: " + response.getStatusCode() + "\n");
				System.out.println(response.asPrettyString() + "\n");
				System.out.println("Id: " + response.jsonPath().getInt("id"));
				System.out.println("------------------------------\n");

			} else if (statusCode == 400) {

				assertEquals(statusCode, response.getStatusCode());
				System.out.println("POST: Erro nas regras de parâmetros\n");
				System.out.println("Status code: " + response.getStatusCode() + "\n");
				System.out.println(response.asPrettyString());
				System.out.println("------------------------------\n");

			} 

		} else if (passo == "put") {
			
			System.out.println("Teste simulações:\n");

			Response response = request.put("http://localhost:8080/api/v1/simulacoes/97093236010");
			assertEquals(statusCode, response.getStatusCode());
			System.out.println("PUT: Cpf sem simulação\n");
			System.out.println("Status code: " + response.getStatusCode() + "\n");
			System.out.println(response.asPrettyString());
			System.out.println("------------------------------\n");

		}

	}

	public void restricoesGET(String cpf, int statusCode) {

		System.out.println("------------------------------\n");
		System.out.println("Teste restrições:\n");

		Response response = RestAssured.get("http://localhost:8080/api/v1/restricoes/" + cpf);
		assertEquals(statusCode, response.getStatusCode());
		System.out.println("GET: Cpf sem restrição\n");
		System.out.println("Status code: " + response.getStatusCode());
		System.out.println("------------------------------\n");

	}

	public void simulacoesGET2(String cpf, int statusCode) {
		
		System.out.println("Teste simulações:\n");

		Response response = RestAssured.get("http://localhost:8080/api/v1/simulacoes/" + cpf);
		assertEquals(statusCode, response.getStatusCode());
		System.out.println("GET: Cpf sem simulação\n");
		System.out.println("Status code: " + response.getStatusCode() + "\n");
		System.out.println(response.asPrettyString());
		System.out.println("------------------------------\n");

	}

}
