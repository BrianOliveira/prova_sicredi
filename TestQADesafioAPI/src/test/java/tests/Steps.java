package tests;

import pages.Metodos;

public class Steps {

	Metodos metodo = new Metodos();
	
	public void cpfSemRestricoes11() {

		metodo.restricoesGET("99999999999", 204);

	}

	public void validarCadastroSucesso() {

		metodo.parametersPostPut("Fulano de Tal", "97093236014", "email@email.com", 1200, 3, true, "post", 201);

	}

	public void validarFalhaRegra() {

		metodo.parametersPostPut("Fulano de Tal", "97093236016", "email@email.com", 1200, 0, true, "post", 400);

	}

	public void validarCpfNaoEncontrado() {

		metodo.parametersPostPut("Fulano de Tal", "97093236014", "email@email.com", 1200, 3, true, "put", 404);

	}

	public void validarCpfSemSimulacao() {

		metodo.simulacoesGET2("97093236010", 404);

	}

}
