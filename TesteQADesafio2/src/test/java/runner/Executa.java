package runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tests.Steps;

public class Executa extends Steps {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		metodo.abrirNavegador("Iniciando Teste",
				"https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		metodo.fecharNavegador("Finalizando Teste");

	}

	@Test
	public void testA() {

		mudarValorCombo();
		clicarBotaoAddCustomer();
		formularioName();
		formularioLastName();
		formularioContactFirstName();
		formularioPhone();
		formularioAddressLine1();
		formularioAddressLine2();
		formularioCity();
		formularioState();
		formularioPostalCode();
		formularioCountry();
		formularioFromEmployeer();
		formularioCreditLimit();
		clicarBotaoSave();
		validarMensagem();

	}

	@Test
	public void testB() throws InterruptedException {

		clicarGoBackToList();
		clicarDigitar();
		clicarCheckBox();
		esperarEstabilizarPagina();
		clicarCheckBox();
		delete1();
		validarTexto1();
		delete2();
		esperarEstabilizarPagina();
		validarTexto2();

	}

}
