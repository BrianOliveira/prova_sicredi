package tests;

import elementos.Elementos;
import pages.Metodos;

public class Steps {

	protected static Metodos metodo = new Metodos();
	protected static Elementos codLink = new Elementos();

	public void mudarValorCombo() {

		metodo.selector("Bootstrap V4 Theme", codLink.getSelectVersion());

	}

	public void clicarBotaoAddCustomer() {

		metodo.click(codLink.getAddCustomer());

	}

	public void formularioName() {

		metodo.click(codLink.getName());
		metodo.escrever("Teste Sicredi", codLink.getName());

	}

	public void formularioLastName() {

		metodo.click(codLink.getLastName());
		metodo.escrever("Teste", codLink.getLastName());

	}

	public void formularioContactFirstName() {

		metodo.click(codLink.getContactFirstName());
		metodo.escrever("seu nome", codLink.getContactFirstName());

	}

	public void formularioPhone() {

		metodo.click(codLink.getPhone());
		metodo.escrever("51 9999-9999", codLink.getPhone());

	}

	public void formularioAddressLine1() {

		metodo.click(codLink.getAddressLine1());
		metodo.escrever("Av Assis Brasil, 3970", codLink.getAddressLine1());

	}

	public void formularioAddressLine2() {

		metodo.click(codLink.getAddressLine2());
		metodo.escrever("Torre D", codLink.getAddressLine2());

	}

	public void formularioCity() {

		metodo.click(codLink.getCity());
		metodo.escrever("Porto Alegre", codLink.getCity());

	}

	public void formularioState() {

		metodo.click(codLink.getState());
		metodo.escrever("RS", codLink.getState());

	}

	public void formularioPostalCode() {

		metodo.click(codLink.getPostalCode());
		metodo.escrever("91000-000", codLink.getPostalCode());

	}

	public void formularioCountry() {

		metodo.click(codLink.getCountry());
		metodo.escrever("Brasil", codLink.getCountry());

	}

	public void formularioFromEmployeer() {

		metodo.click(codLink.getFromEmployeer());
		metodo.escrever("Fixter", codLink.getFromEmployeer());

	}

	public void formularioCreditLimit() {

		metodo.click(codLink.getCreditLimit());
		metodo.escrever("200", codLink.getCreditLimit());

	}

	public void clicarBotaoSave() {

		metodo.click(codLink.getSave());

	}

	public void validarMensagem() {

		metodo.validarTexto("Your data has been successfully stored into the database. Edit Record or Go back to list",
				codLink.getValidarMensagem());

	}

	public void clicarGoBackToList() {

		metodo.click(codLink.getGoBackToList());

	}

	public void clicarDigitar() {

		metodo.click(codLink.getSearchName());
		metodo.escrever("Teste Sicredi", codLink.getSearchName());

	}

	public void clicarCheckBox() {

		metodo.click(codLink.getCheckbox());

	}

	public void esperarEstabilizarPagina() throws InterruptedException {

		metodo.tempoEspera(2000);

	}

	public void delete1() {

		metodo.click(codLink.getDelete1());

	}

	public void validarTexto1() {

		metodo.validarTexto("Are you sure that you want to delete this 1 item?", codLink.getValidarTexto1());

	}

	public void delete2() {

		metodo.click(codLink.getDelete2());

	}

	public void validarTexto2() {

		metodo.validarTexto("Your data has been successfully deleted from the database.", codLink.getValidarTexto2());

	}

}
