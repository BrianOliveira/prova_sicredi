package pages;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Metodos {

	WebDriver driver;

	public void abrirNavegador(String passo, String site) {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(site);
		driver.manage().window().maximize();

	}

	public void escrever(String texto, By element) {

		driver.findElement(element).sendKeys(texto);

	}

	public void selector(String texto, By element) {

		WebElement selectElement = driver.findElement(element);
		Select selectObject = new Select(selectElement);
		selectObject.selectByVisibleText(texto);

	}

	public void click(By element) {

		driver.findElement(element).click();

	}

	public void tempoEspera(int tempo) throws InterruptedException {

		Thread.sleep(tempo);

	}

	public void validarTexto(String textoEsperado, By element) {

		WebElement elemento = new WebDriverWait(driver, Duration.ofSeconds(10))
				.until(driver -> driver.findElement(element));
		assertEquals(elemento.getText(), textoEsperado);

	}

	public void fecharNavegador(String passo) {

		driver.quit();

	}

}
