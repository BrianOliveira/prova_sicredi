package elementos;

import org.openqa.selenium.By;

public class Elementos {

	private By selectVersion = By.id("switch-version-select");
	private By addCustomer = By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[1]/div[1]/a");
	private By name = By.name("customerName");
	private By lastName = By.name("contactLastName");
	private By contactFirstName = By.name("contactFirstName");
	private By phone = By.name("phone");
	private By addressLine1 = By.name("addressLine1");
	private By addressLine2 = By.name("addressLine2");
	private By city = By.name("city");
	private By state = By.name("state");
	private By postalCode = By.name("postalCode");
	private By country = By.name("country");
	private By fromEmployeer = By.name("salesRepEmployeeNumber");
	private By creditLimit = By.name("creditLimit");
	private By save = By.id("form-button-save");
	private By validarMensagem = By.cssSelector("#report-success > p");
	private By goBackToList = By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[15]/div[2]/p/a[2]");
	private By searchName = By.name("customerName");
	private By checkbox = By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[1]/div/input");
	private By validarClicarCheckbox = By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[3]/div[4]/span[1]");
	private By delete1 = By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[2]/div[1]/a");
	private By validarTexto1 = By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]");
	private By delete2 = By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]");
	private By validarTexto2 = By.xpath("/html/body/div[3]/span[3]/p");

	public By getSelectVersion() {
		return selectVersion;
	}

	public By getAddCustomer() {
		return addCustomer;
	}

	public By getName() {
		return name;
	}

	public By getLastName() {
		return lastName;
	}

	public By getContactFirstName() {
		return contactFirstName;
	}

	public By getPhone() {
		return phone;
	}

	public By getAddressLine1() {
		return addressLine1;
	}

	public By getAddressLine2() {
		return addressLine2;
	}

	public By getCity() {
		return city;
	}

	public By getState() {
		return state;
	}

	public By getPostalCode() {
		return postalCode;
	}

	public By getCountry() {
		return country;
	}

	public By getFromEmployeer() {
		return fromEmployeer;
	}

	public By getCreditLimit() {
		return creditLimit;
	}

	public By getSave() {
		return save;
	}

	public By getValidarMensagem() {
		return validarMensagem;
	}

	public By getGoBackToList() {
		return goBackToList;
	}

	public By getSearchName() {
		return searchName;
	}

	public By getCheckbox() {
		return checkbox;
	}

	public By getValidarClicarCheckbox() {
		return validarClicarCheckbox;
	}

	public By getDelete1() {
		return delete1;
	}

	public By getValidarTexto1() {
		return validarTexto1;
	}

	public By getDelete2() {
		return delete2;
	}

	public By getValidarTexto2() {
		return validarTexto2;
	}

}
