# prova_sicredi

<p align="center">Prova técnica de Automação de Teste</p>
<p>Nessa prova contém dois projetos, sendo um de teste automatizado em UI, e o outro teste automatizado em API, ambos foram feitos em Page Objects.</p>

## TesteQADesafio2

## Como Executar Aplicação

Abrir o projeto na IDE Eclipse, clicar em ExecutaDesafioUI, e sua aplicação será executada com sucesso.

## TesteQADesafioAPI

## Pré Requisitos

* Java 8
* JDK deve estar instalado 
* Maven deve estar instalado e configurado no path da aplicação
* Eclipse

## Como Executar Aplicação

```bash
Na raiz do projeto, através de seu Prompt de Commando/Terminal/Console execute o 
comando 

mvn clean spring-boot:run 

A aplicação estará disponível através da URL http://localhost:8080

Você pode trocar a porta da aplicação, caso a 8080 já estiver em uso, adicionando a 
propriedade JVM server.port. 

mvn clean spring-boot:run -Dserver.port=8888 
```

Abrir o projeto na IDE Eclipse, clicar em ExecutaDesafioAPI, e sua aplicação será executada com sucesso.

## Documentação Técnica da Aplicação

A documentação técnica da API está disponível através do OpenAPI/Swagger em 
http://localhost:8080/swagger-ui.html

## Bugs Detectados no Projeto

### RESTRIÇÕES:

### GET: Consultar uma restrição

De acordo com o documento, ao consultar um cpf com restrição, deveria retornar a seguinte mensagem: "O CPF 99999999999 possui restrição", porém está retornando essa "mensagem": "O CPF 58063164083 tem problema".

### SIMULAÇÕES:

### POST: Simulação para o mesmo cpf

De acordo com o documento, ao realizar uma simulação com o mesmo cpf, deveria retornar o status code: 409, e a seguinte mensagem: "CPF já existente", porém,  está retornando status code: 400, e a seguinte "mensagem": "CPF duplicado"

### GET: Consultar todas as simulações

De acordo com o documento, ao consultar simulações e não existir nenhuma cadastrada, retorna status code: 204, porém está retornando status code: 200

### DELETE: Remover uma simulação

De acordo com o documento, ao simular uma remoção com um id não cadastrado, retorna o status code: 404, e a seguinte mensagem: "Simulação não encontrada", porém está retornando status code: 200 e a seguinte mensagem: "ok"

De acordo com o documento, ao simular uma remoção com um id cadastrado, retorna o status code 204, porém está retornando status code: 200

Observação: Só será possivel realizar a execução do teste de API uma vez, para realizar mais de uma vez, será necessário executar a simulação DELETE no "Id' gerado da ultima simulação.

Feito por Brian Oliveira [Acesse meu linkedin](https://www.linkedin.com/in/brian-oliveira-385356122/)